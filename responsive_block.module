<?php

/**
 * @file
 */

const RESPONSIVE_BLOCK_NOT_RESTRICT = 0;
const RESPONSIVE_BLOCK_HIDE = 1;



/**
 *
 * Block visibility options for mobile devices using mobile_detect detection.
 */

/**
 * Implements hook_form_FORMID_alter().
 *
 * @see block_add_block_form()
 */
function responsive_block_form_block_add_block_form_alter(&$form, &$form_state) {
  responsive_block_form_block_admin_configure_alter($form, $form_state);
}

/**
 * Implements hook_form_FORMID_alter().
 * Adds mobile specific visibility options to block configuration form.
 *
 * @see block_admin_configure()
 */
function responsive_block_form_block_admin_configure_alter(&$form, &$form_state) {
  $device_visibility = db_query("SELECT desktop_visibility,mobile_visibility,tablet_visibility FROM {responsive_block} WHERE module = :module AND delta = :delta", array(
    ':module' => $form['module']['#value'],
    ':delta' => $form['delta']['#value'],
  ))->fetchAssoc();

  if ($device_visibility) {
    $desktop_visibility = $device_visibility['desktop_visibility'];
    $mobile_visibility = $device_visibility['mobile_visibility'];
    $tablet_visibility = $device_visibility['tablet_visibility'];
  }
  else {
    $desktop_visibility = RESPONSIVE_BLOCK_NOT_RESTRICT;
    $mobile_visibility = RESPONSIVE_BLOCK_NOT_RESTRICT;
    $tablet_visibility = RESPONSIVE_BLOCK_NOT_RESTRICT;
  }

  $form['visibility']['responsive_block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mobile Detect'),
    '#group' => 'visibility',
    '#weight' => 6,
    '#tree' => TRUE,
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'responsive_block') . '/responsive_block.js'),
    ),
  );

  $form['visibility']['responsive_block']['desktop_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Hide or show this block on desktop   devices'),
    '#default_value' => $desktop_visibility,
    '#options' => array(
      RESPONSIVE_BLOCK_NOT_RESTRICT => t('Not Restrict'),
      RESPONSIVE_BLOCK_HIDE => t('Hide'),
    ),
  );

  $form['visibility']['responsive_block']['tablet_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Hide or show this block on tablet devices'),
    '#default_value' => $tablet_visibility,
    '#options' => array(
      RESPONSIVE_BLOCK_NOT_RESTRICT => t('Not restricted'),
      RESPONSIVE_BLOCK_HIDE => t('Hide'),
    ),
  );

  $form['visibility']['responsive_block']['mobile_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Hide or show this block on smalltouch mobile devices'),
    '#default_value' => $mobile_visibility,
    '#options' => array(
      RESPONSIVE_BLOCK_NOT_RESTRICT => t('Not restricted'),
      RESPONSIVE_BLOCK_HIDE => t('Hide'),
    ),
  );

  $form['#submit'][] = 'responsive_block_form_block_admin_configure_submit';
}

/**
 * Form submit handler for block configuration form.
 *
 * @see responsive_block_form_block_admin_configure_alter()
 */
function responsive_block_form_block_admin_configure_submit($form, &$form_state) {
  db_delete('responsive_block')
    ->condition('module', $form_state['values']['module'])
    ->condition('delta', $form_state['values']['delta'])
    ->execute();

  $device_visibility = $form_state['values']['responsive_block'];

  if ($device_visibility['mobile_visibility'] || $device_visibility['tablet_visibility'] || $device_visibility['desktop_visibility']) {

    $query = db_insert('responsive_block')->fields(array(
      'mobile_visibility',
      'tablet_visibility',
      'desktop_visibility',
      'module',
      'delta',
    ));
    $query->values(array(
      'mobile_visibility' => $device_visibility['mobile_visibility'],
      'tablet_visibility' => $device_visibility['tablet_visibility'],
      'desktop_visibility' => $device_visibility['desktop_visibility'],
      'module' => $form_state['values']['module'],
      'delta' => $form_state['values']['delta'],
    ));
    $query->execute();
  }
}

/**
 * Implements hook_block_list_alter().
 * Check the mobile detect specific visibilty settings.
 * Remove the block if the visibility conditions are not met.
 */
function responsive_block_block_list_alter(&$blocks) {
  global $theme_key;

  if (function_exists('mobile_detect_get_object') && class_exists('Mobile_Detect')) {
    $detect = mobile_detect_get_object();
    // Boolean false/true.
    $is_mobile = $detect->isMobile();
    // Boolean false/true.
    $is_tablet = $detect->isTablet();
    // Boolean false/true.
    $is_desktop = !$is_mobile && !$is_tablet;
  }
  else {
    return;
  }

  $device_visibility = db_select("responsive_block")
    ->fields('responsive_block')
    ->execute()
    ->fetchAll();

  $visibilities = array();
  foreach ($device_visibility as $v) {
    $visibilities[$v->module][$v->delta] = $v;
  }

  foreach ($blocks as $i => $block) {
    if (isset($visibilities[$block->module][$block->delta])) {
      if ($is_desktop) {
        if ($visibilities[$block->module][$block->delta]->desktop_visibility == RESPONSIVE_BLOCK_HIDE) {
          unset($blocks[$i]);
        }
      }
      elseif ($is_tablet) {
        if ($visibilities[$block->module][$block->delta]->tablet_visibility == RESPONSIVE_BLOCK_HIDE) {
          unset($blocks[$i]);
        }
      }
      elseif ($is_mobile) {
        if ($visibilities[$block->module][$block->delta]->mobile_visibility == RESPONSIVE_BLOCK_HIDE) {
          unset($blocks[$i]);
        }
      }
    }
  }
}
