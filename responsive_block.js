(function ($) {

/**
 * Provide the summary information for the mobile_detect Block settings
 * vertical tab.
 */
Drupal.behaviors.ResponsiveBlockSettingsSummary = {
  attach: function (context) {

    $('fieldset#edit-responsive-block', context).drupalSetSummary(function (context) {
      var vals = [];
      $('input[type="radio"]:checked', context).each(function () {
        vals.push($.trim($(this).next('label').text()));
      });
      if (!vals.length) {
        vals.push(Drupal.t('Not restricted'));
      }
      return vals.join(', ');
    });

  }
};

})(jQuery);
